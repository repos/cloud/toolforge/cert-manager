This repo was incorporated in the toolforge-deploy repository:

https://gitlab.wikimedia.org/repos/cloud/toolforge/toolforge-deploy

New changes should go there instead.
